package com.gigigotrek.dummyjavaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.gigigotrek.dummyjavaspring.repository")
@EntityScan("com.gigigotrek.dummyjavaspring.data")
@ComponentScan(basePackages = {"com.gigigotrek.dummyjavaspring",
                "com.gigigotrek.dummyjavaspring.data",
                "com.gigigotrek.dummyjavaspring.repository"})
@SpringBootApplication
public class DummyJavaSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyJavaSpringApplication.class, args);
	}
}
